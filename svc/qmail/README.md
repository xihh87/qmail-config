![Build Status](https://gitlab.com/xihh87/qmail-config/badges/main/pipeline.svg)

`registry.gitlab.com/xihh87/qmail-config/qmail`

Quick reference
---------------

Maintained by: [Joshua Haase](https://gitlab.com/xihh87 )

Environment Variables
---------------------

`HOSTNAME`

This variable is *required* to setup the FQDN of your qmail hostname.

How to use this image
---------------------

### start a qmail instance

```
docker run -d \
    -e HOSTNAME=mail.example.com \
    -v /home:/home \
    registry.gitlab.com/xihh87/qmail-config/qmail:master
```

### … via `compose.yml`:

```
version: '3.1'

sservices:
  smtp:
    image: registry.gitlab.com/xihh87/qmail-config/qmail:master
    environment:
      - HOSTNAME=mail.example.com
    volumes:
      - queue:/var/lib/qmail/queue
      - mail:/home/

volumes:
  queue:
  mail:
```
