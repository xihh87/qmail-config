help:QV: ## show this command
	cmd/show-help mkfile | sort

start:QV: ## setup working environment
	virtualenv .venv
	. .venv/bin/activate
	python -m pip install --upgrade pip
	python -m pip install -r requirements.txt

ANSIBLE_ARGS="-v"
ANSIBLE_FORCE_COLOR=yes

s6-on-systemd:QV:   skarnet   ## install skarnet tools in localhost
	. .venv/bin/activate
	ansible-playbook ${ANSIBLE_ARGS} playbooks/s6-on-systemd.yml 2>&1 \
	| cmd/log-and-show

djbdns:QV: ## install djbdns en el servidor
	. .venv/bin/activate
	ansible-playbook ${ANSIBLE_ARGS} playbooks/dnscache-install.arch.yml 2>&1 \
	| cmd/log-and-show

skarnet:QV: ## install skarnet tools in localhost
	. .venv/bin/activate
	ansible-playbook ${ANSIBLE_ARGS} playbooks/skarnet-tools.yml 2>&1 \
	| cmd/log-and-show

qmail:QV:	s6-on-systemd	djbdns	## execute the qmail module in localhost
	. .venv/bin/activate
	ansible-playbook ${ANSIBLE_ARGS} playbooks/qmail-install.arch.yml 2>&1 \
	| cmd/log-and-show

docker:QV:	## build a docker image
	set -x
	export VERSION="$(git rev-parse --short HEAD)$(cmd/is-git-clean)"
	docker compose build | cmd/log-and-show
	BRANCH="$(cmd/current-branch)$(cmd/is-git-clean)"
	docker tag \
		registry.gitlab.com/xihh87/qmail-config/qmail:${VERSION} \
		registry.gitlab.com/xihh87/qmail-config/qmail:${BRANCH}
	docker tag \
		registry.gitlab.com/xihh87/qmail-config/dnscache:${VERSION} \
		registry.gitlab.com/xihh87/qmail-config/dnscache:${BRANCH}

push:V:	## push the latest docker image to gitlab
	set -x
	BRANCH="$(cmd/current-branch)"
	docker push registry.gitlab.com/xihh87/qmail-config/qmail:${BRANCH}
